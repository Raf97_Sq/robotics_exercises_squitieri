clear all;
close all;
clc;

filepath = 'data/circle1.traj';
rho = 0.3;
center = [-1.11 0 1.465-rho];

no_of_cp = 16;
no_of_samples = 200;

start_angle = pi/2;
theta = linspace(start_angle, start_angle+2*pi, no_of_cp);

ctrl_points = NaN * ones(3, no_of_cp);
ctrl_points(1,:) = center(1) * ones(1, no_of_cp);
ctrl_points(2,:) = center(2) + rho * cos(theta);
ctrl_points(3,:) = center(3) + rho * sin(theta);

[x_lambda, lambda] = generate_path(ctrl_points, no_of_samples, true);

x_lambda(4,:) = zeros(1, no_of_samples);
x_lambda(5,:) = -pi/2 * ones(1, no_of_samples);
x_lambda(6,:) = pi * ones(1, no_of_samples);

export_ros_workspace_path(filepath, lambda, x_lambda);

