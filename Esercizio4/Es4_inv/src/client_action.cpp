#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/robot_state.h>
#include <Es4_inv/mactAction.h>
#include <moveit/robot_state/conversions.h>
#include <angles/angles.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

void handleGoalCompletionEvent(const actionlib::SimpleClientGoalState &state, const Es4_inv::mactResultConstPtr &result);
void serializeIKSolution(std::ostringstream & ss, const moveit_msgs::RobotState & robot_state);
void handleFeedbackEvent(const Es4_inv::mactFeedbackConstPtr & feedback);
void handleGoalActiveEvent();


int main(int argc , char **argv){

    ros::init(argc,argv,"kinematics_action_client");

    actionlib::SimpleActionClient<Es4_inv::mactAction> client("ik_solver",true);

    client.waitForServer();

    Es4_inv::mactGoal goal;

    goal.end_effector_pose.position.x=1.0;
    goal.end_effector_pose.position.y=0.0;
    goal.end_effector_pose.position.z=1.0;

    tf2::Quaternion quaternion;
    quaternion.setRPY(0.0, 0.0, 0.0);

    goal.end_effector_pose.orientation = tf2::toMsg(quaternion);

    client.sendGoal(goal,&handleGoalCompletionEvent,&handleGoalActiveEvent,&handleFeedbackEvent);
    if(!client.waitForResult(ros::Duration(30.0)))
    ROS_ERROR("The IK solver did not complete in the allowed time");
    //std::unique_lock<std::mutex lock(mutex);
    //result_handled.wait(lock);
    ros::shutdown();
    return 0;


}


void serializeIKSolution(std::ostringstream & ss, const moveit_msgs::RobotState & robot_state)
{
    int n_joints=robot_state.joint_state.position.size();
    ss <<"[";

    for(int i=0;i<n_joints;i++){

        ss<< robot_state.joint_state.position[i];
        if(i != n_joints-1)
            ss<<", ";
    }
    ss<<"]";
}

void handleFeedbackEvent(const Es4_inv::mactFeedbackConstPtr & feedback)
{
    std::ostringstream ss;
    ss << "Received IK solution: ";
    serializeIKSolution(ss,feedback->ik_solution);
    ROS_INFO_STREAM(ss.str());
}

void handleGoalActiveEvent(){
    ROS_INFO("Inverse kinematics request sent to the IK resolution action server");

}

void handleGoalCompletionEvent(const actionlib::SimpleClientGoalState &state, const Es4_inv::mactResultConstPtr &result)
{
    std::ostringstream ss;
    if(state==actionlib::SimpleClientGoalState::StateEnum::SUCCEEDED){
    int num_of_solutions= result->ik_solutions.size();
   ss <<"Goal achieved. "<< state.getText()<<std::endl;
    for(int i=0;i<num_of_solutions;i++)
    {
        serializeIKSolution(ss,result->ik_solutions[i]);
        ss <<std::endl;
    }
    ROS_INFO_STREAM(ss.str());
    ros::NodeHandle nh;
    
    ros::Publisher joint_state_publisher= nh.advertise<sensor_msgs::JointState>("joint_states",1);
  

  robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
  robot_model::RobotModelConstPtr kinematic_model = robot_model_loader.getModel();
  const robot_state::JointModelGroup* joint_model_group= kinematic_model->getJointModelGroup("fanuc_groups");
  sensor_msgs::JointState joint_state_msg;
    joint_state_msg.name=joint_model_group->getVariableNames();
    ROS_INFO("Publishing solutions...");
    ros::Duration sleep_time(2.0);
    for(int i=0;i<num_of_solutions;i++){
        sleep_time.sleep();
        joint_state_msg.position=result->ik_solutions[i].joint_state.position;
        joint_state_msg.header.stamp=ros::Time::now();
        joint_state_publisher.publish(joint_state_msg);

    }
    ROS_INFO("All solution publish");
    }else{
        ss<<"Goal aborted."<<state.getText();
        ROS_INFO_STREAM(ss.str());
    }

    }
    