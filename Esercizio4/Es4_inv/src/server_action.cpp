#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/robot_state.h>
#include <Es4_inv/mactAction.h>
#include <moveit/robot_state/conversions.h>
#include <angles/angles.h>
#include <eigen_conversions/eigen_msg.h>




class InverseKinematicsAction {

//Creazione attributi classe
private:
 ros::NodeHandle nh_;

 actionlib::SimpleActionServer<Es4_inv::mactAction> action_server_;
 Es4_inv::mactResult result_;

 std::vector<std::vector<double>> ik_solutions_;
 robot_model_loader::RobotModelLoaderConstPtr robot_model_loader_;
 robot_model::RobotModelConstPtr kinematic_model_;
 const robot_state::JointModelGroup * jgm_;



//Inizializzazione attributi classe tramite costruttore e distruttore
public:
InverseKinematicsAction():
action_server_(nh_,"ik_solver",boost::bind(&InverseKinematicsAction::ik_callback_,this,_1),false),
robot_model_loader_(new robot_model_loader::RobotModelLoader("robot_description")),
kinematic_model_(robot_model_loader_->getModel()),
jgm_(nullptr)
{
jgm_= kinematic_model_->getJointModelGroup("fanuc_groups");

action_server_.start();

}


~InverseKinematicsAction(){

}

//Definizione metodi di utils e callback
private:


 bool isSolutionNew_(const std::vector<double> & solution) const{

     for(int i=0 ; i< ik_solutions_.size();i++){ //ik_solutions è un vettore di vettori (mi fisso una configurazione)
         bool are_solutions_equal=true;


    for(int j=0;j<ik_solutions_[i].size()&& are_solutions_equal;j++) //Scorro la configurazione fissata prima
    {
     double diff;
     if(jgm_->getActiveJointModels()[j]->getType()==robot_model::JointModel::REVOLUTE)  //Se il giunto che sto controllando è rotazionale
     {
         diff=angles::shortest_angular_distance(ik_solutions_[i][j],solution[j]); //Devo fare un controllo per vedere se angolarmente sono uguali i due giunti 
        //Devo fare un controllo sulla minima distanza angolare giunto per giunto
    }else{//Se il giunto è di tipo prismatico faccio la differenza semplice
        diff=ik_solutions_[i][j]-solution[j];
      } 

        if(std::fabs(diff) > 1e-3)//Se la differenza è maggiore le soluzioni che sto confrontando son diverse
            are_solutions_equal=false;

    }

    if(are_solutions_equal) //Se le soluzioni finora confrontate sono uguali vuol dire che la soluzione NON è nuova quindi ritorno False
                            // se invece non è così continuo il confronto con un'altra soluzione presente nel vettori soluzioni
        return false;

    }
    return true; 
}
 
 void normalizeJointPositions_(std::vector<double> & solution) const{

     for (int i=0;i<solution.size();i++){

         if(jgm_->getActiveJointModels()[i]->getType() == robot_model::JointModel::REVOLUTE){
             solution[i] = angles::normalize_angle(solution[i]);
         }

     }

}


std::vector<double> generateSeedState_()const{
     std::vector<double> seed_state;
     std::vector<std::string> joint_names = kinematic_model_->getVariableNames();
     for (int i=0;i<joint_names.size();i++){
         double lb=kinematic_model_->getURDF()->getJoint(joint_names[i])->limits->lower; //Righe 109 e 110 servono per accedere al limite del giunto attuale tramite URDF
         double ub=kinematic_model_->getURDF()->getJoint(joint_names[i])->limits->upper;
         double span=ub-lb;
         seed_state.push_back((double)std::rand()/RAND_MAX*span+lb);//Il seed è una configurazione di giunti
         //Con la funzione rand per genereare un numero compreso tra 4 e 7 , si fa rand*(7-4)+4 -> rand*(lunghezza_range)+lower_bound
     }
     return seed_state;
 }



 void ik_callback_(const Es4_inv::mactGoalConstPtr & goal){
     //Definizione solver cinematico
     const kinematics::KinematicsBaseConstPtr solver= jgm_->getSolverInstance();
     Es4_inv::mactResult result;

     int ik_calls_counter=0;
    
     while(ik_calls_counter<200 && ros::ok()){

         std::vector<double> seed_state= generateSeedState_();
         std::vector<double> solution;
         moveit_msgs::MoveItErrorCodes error_code;

        solver->getPositionIK(goal->end_effector_pose,seed_state,solution,error_code);

        if(error_code.val == moveit_msgs::MoveItErrorCodes::SUCCESS){
            normalizeJointPositions_(solution);

            if(isSolutionNew_(solution)){
                ik_solutions_.push_back(solution);
                moveit::core::RobotState robot_state(kinematic_model_);

                robot_state.setVariablePositions(solution);

                Es4_inv::mactFeedback feedback;
                moveit::core::robotStateToRobotStateMsg(robot_state,feedback.ik_solution);
                action_server_.publishFeedback(feedback);
                result.ik_solutions.push_back(feedback.ik_solution);

    
        }

     }
     ik_calls_counter++;
}

    if(ik_solutions_.size()==0)
        action_server_.setAborted(result,"Could not find any IK solution");
    else{
        std::ostringstream ss;
        ss << "Found " << ik_solutions_.size() << " IK solutions";

        action_server_.setSucceeded(result,ss.str());
    }
    ik_solutions_.resize(0);
 }


};



int main(int argc, char **argv)
{
  ros::init(argc, argv, "Action_server");
  InverseKinematicsAction action_msg();
  ros::spin();
  return 0;
}