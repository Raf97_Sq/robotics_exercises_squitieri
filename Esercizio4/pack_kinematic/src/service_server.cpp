#include <ros/ros.h>
#include <moveit_msgs/GetPositionFK.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#include <std_msgs/String.h>
#include <math.h>
#include <pack_kinematic/servicemsg.h>


bool calculate(pack_kinematic::servicemsg::Request &request,
pack_kinematic::servicemsg::Response &response)
{

    robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
    robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
    ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());
    robot_state::RobotStatePtr kinematic_state(new robot_state::RobotState(kinematic_model));
    kinematic_state->setToDefaultValues();
    const robot_state::JointModelGroup* joint_model_group = kinematic_model->getJointModelGroup("fanuc_groups");
    const std::vector<std::string>& joint_names = joint_model_group->getVariableNames();
    kinematic_state->setJointGroupPositions(joint_model_group,{0,0,0,0,0,0});
    //kinematic_state->se(joint_model_group);
    const Eigen::Isometry3d& end_effector_state = kinematic_state->getGlobalLinkTransform("flange");


    
    
    float w_quaternione=0.5*sqrt(end_effector_state.rotation()(0,0)+end_effector_state.rotation()(1,1)+end_effector_state.rotation()(2,2)+1);
    float x_quaternione=0;
    float y_quaternione=0;
    float z_quaternione=0;

    if(end_effector_state.rotation()(2,1)-end_effector_state.rotation()(1,2)>0){
        x_quaternione=sqrt(end_effector_state.rotation()(0,0)-end_effector_state.rotation()(1,1)-end_effector_state.rotation()(2,2)+1);
    }else{ 
        x_quaternione=-sqrt(end_effector_state.rotation()(0,0)-end_effector_state.rotation()(1,1)-end_effector_state.rotation()(2,2)+1);
    }


    if(end_effector_state.rotation()(0,2)-end_effector_state.rotation()(2,0)>0){
        y_quaternione=sqrt(end_effector_state.rotation()(1,1)-end_effector_state.rotation()(2,2)-end_effector_state.rotation()(0,0)+1);
    }else{ 
        y_quaternione=-sqrt(end_effector_state.rotation()(1,1)-end_effector_state.rotation()(2,2)-end_effector_state.rotation()(0,0)+1);
    }


    if(end_effector_state.rotation()(1,0)-end_effector_state.rotation()(0,1)>0){
        z_quaternione=sqrt(end_effector_state.rotation()(2,2)-end_effector_state.rotation()(0,0)-end_effector_state.rotation()(1,1)+1);
    }else{ 
        z_quaternione=-sqrt(end_effector_state.rotation()(2,2)-end_effector_state.rotation()(0,0)-end_effector_state.rotation()(1,1)+1);
    }

    


    response.x_quaternion=0.5*x_quaternione;
    response.y_quaternion=0.5*y_quaternione;
    response.z_quaternion=0.5*z_quaternione;
    response.w_quaternion=w_quaternione;
    response.x_translation=end_effector_state.translation().x();
    response.y_translation=end_effector_state.translation().y();
    response.z_translation=end_effector_state.translation().z();
    

    return true;
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "Calculate_FK_server");
    ros::NodeHandle nh;
    ros::ServiceServer service = nh.advertiseService("Calculate_FK", calculate);
    ros::spin();
    return 0;
}