#include <ros/ros.h>
#include <moveit_msgs/GetPositionFK.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#include <std_msgs/String.h>
#include <moveit/transforms/transforms.h>
#include <moveit_msgs/RobotState.h>
#include <moveit_msgs/RobotTrajectory.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "Compute_FK_client");
    ros::NodeHandle nh;
    ros::ServiceClient client =
    nh.serviceClient<moveit_msgs::GetPositionFK>("compute_fk");
    moveit_msgs::GetPositionFK service;
    service.request.fk_link_names={"flange"};
   // service.request.robot_state={    joint_state:{    position:[0, 0, 0, 0, 0, 0]}    };
    service.request.robot_state.joint_state.position={0, 0, 0, 0, 0, 0};
    /*OR BY COMMAND LINE:
    rosservice call /compute_fk "{    header:{    }    , fk_link_names:[flange], 
    robot_state:{    joint_state:{    position:[0, 0, 0, 0, 0, 0]}    }    }    "

    */
        if (client.call(service)) {
        ROS_INFO_STREAM("Translation: \n" << service.response.pose_stamped[0].pose.position << "\n");
        ROS_INFO_STREAM("Rotation: \n" << service.response.pose_stamped[0].pose.orientation << "\n");        
        
        } else {
        ROS_ERROR("Failed to call service ");
        return 1;
        }
    return 0;


}