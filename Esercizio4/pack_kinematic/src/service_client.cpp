#include <ros/ros.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#include <pack_kinematic/servicemsg.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "Calculate_FK_client");
    ros::NodeHandle nh;
    ros::ServiceClient client =
    nh.serviceClient<pack_kinematic::servicemsg>("Calculate_FK");
    pack_kinematic::servicemsg service;

   
    
    
        if (client.call(service)) {
        tf2::Quaternion tfq((float)service.response.x_quaternion,
       (float)service.response.y_quaternion,(float)service.response.z_quaternion,(float)service.response.w_quaternion);
       
        ROS_INFO("Quaternione---> x=%f ,y=%f ,z=%f,w=%f",(float)service.response.x_quaternion,
       (float)service.response.y_quaternion,(float)service.response.z_quaternion,(float)service.response.w_quaternion);
        tf2::Matrix3x3 m(tfq);
        ROS_INFO("Matrice di rotazione :\n");
        tf2::Vector3 c1=m.getColumn(0);
        tf2::Vector3 c2=m.getColumn(1);
        tf2::Vector3 c3=m.getColumn(2);
        ROS_INFO("%f  %f  %f\n",c1.x(),c2.x(),c3.x());
        ROS_INFO("%f  %f  %f\n",c1.y(),c2.y(),c3.y());
        ROS_INFO("%f  %f  %f\n",c1.z(),c2.z(),c3.z());
        ROS_INFO("\nTraslazione:\n");
        ROS_INFO("[%f;%f;%f]",(float)service.response.x_translation,(float)service.response.y_translation,(float)service.response.z_translation);
        } else {
        ROS_ERROR("Failed to call service ");
        return 1;
        }
    return 0;


}