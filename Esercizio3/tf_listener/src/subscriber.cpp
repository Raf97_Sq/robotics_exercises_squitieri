#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>


int main(int argc, char** argv) {

ros::init(argc, argv, "tf2_listener");
ros::NodeHandle nodeHandle;
tf2_ros::Buffer tfBuffer;
tf2_ros::TransformListener tfListener(tfBuffer);
ros::Rate rate(10.0);


while (nodeHandle.ok()) {
geometry_msgs::TransformStamped transformStamped[7];

try {
transformStamped[0] = tfBuffer.lookupTransform("flange","link6", ros::Time(0));
ROS_INFO_STREAM("--------------------------Trasformazione da link 6 a flangia--------------------------\n");


ROS_INFO_STREAM("Vettore traslazione: " << transformStamped[0].transform.translation);

tf2::Quaternion tfq(transformStamped[0].transform.rotation.x,transformStamped[0].transform.rotation.y,transformStamped[0].transform.rotation.z,transformStamped[0].transform.rotation.w);
tf2::Matrix3x3 m(tfq);






ROS_INFO("Matrice di rotazione :\n");
tf2::Vector3 c1=m.getColumn(0);
tf2::Vector3 c2=m.getColumn(1);
tf2::Vector3 c3=m.getColumn(2);
ROS_INFO("%f  %f  %f\n",c1.x(),c2.x(),c3.x());
ROS_INFO("%f  %f  %f\n",c1.y(),c2.y(),c3.y());
ROS_INFO("%f  %f  %f\n",c1.z(),c2.z(),c3.z());


double r,y,p;
m.getEulerYPR(y,p,r);
ROS_INFO("\nAngoli di eulero (Radianti) y >%f<, p >%f<, r >%f<\n", y, p, r);


tf2::Vector3 var=tfq.getAxis();
ROS_INFO("Rappresentazione asse-angolo:\n");
ROS_INFO("ASSE: [%f  %f  %f]:",var.x(),var.y(),var.z());
ROS_INFO("\nANGOLO:%f",tfq.getAngle());

//---
transformStamped[1] = tfBuffer.lookupTransform("flange","link5", ros::Time(0));
ROS_INFO_STREAM("--------------------------Trasformazione da link 5 a flangia--------------------------\n");


ROS_INFO_STREAM("Vettore traslazione: " << transformStamped[1].transform.translation);

tf2::Quaternion tfq1(transformStamped[1].transform.rotation.x,transformStamped[1].transform.rotation.y,transformStamped[1].transform.rotation.z,transformStamped[1].transform.rotation.w);

tf2::Matrix3x3 m1(tfq1);


ROS_INFO("Matrice di rotazione :\n");
c1=m1.getColumn(0);
c2=m1.getColumn(1);
c3=m1.getColumn(2);
ROS_INFO("%f  %f  %f\n",c1.x(),c2.x(),c3.x());
ROS_INFO("%f  %f  %f\n",c1.y(),c2.y(),c3.y());
ROS_INFO("%f  %f  %f\n",c1.z(),c2.z(),c3.z());



m1.getEulerYPR(y,p,r);
ROS_INFO("\nAngoli di eulero (Radianti) y >%f<, p >%f<, r >%f<\n", y, p, r);


var=tfq1.getAxis();
ROS_INFO("Rappresentazione asse-angolo:\n");
ROS_INFO("ASSE: [%f  %f  %f]:",var.x(),var.y(),var.z());
ROS_INFO("\nANGOLO:%f",tfq1.getAngle());
//----
transformStamped[2] = tfBuffer.lookupTransform("flange","link4", ros::Time(0));
ROS_INFO_STREAM("--------------------------Trasformazione da link 4 a flangia--------------------------\n");


ROS_INFO_STREAM("Vettore traslazione: " << transformStamped[2].transform.translation);

tf2::Quaternion tfq2(transformStamped[2].transform.rotation.x,transformStamped[2].transform.rotation.y,transformStamped[2].transform.rotation.z,transformStamped[2].transform.rotation.w);

tf2::Matrix3x3 m2(tfq2);


ROS_INFO("Matrice di rotazione :\n");
c1=m2.getColumn(0);
c2=m2.getColumn(1);
c3=m2.getColumn(2);
ROS_INFO("%f  %f  %f\n",c1.x(),c2.x(),c3.x());
ROS_INFO("%f  %f  %f\n",c1.y(),c2.y(),c3.y());
ROS_INFO("%f  %f  %f\n",c1.z(),c2.z(),c3.z());



m2.getEulerYPR(y,p,r);
ROS_INFO("\nAngoli di eulero (Radianti) y >%f<, p >%f<, r >%f<\n", y, p, r);


var=tfq2.getAxis();
ROS_INFO("Rappresentazione asse-angolo:\n");
ROS_INFO("ASSE: [%f  %f  %f]:",var.x(),var.y(),var.z());
ROS_INFO("\nANGOLO:%f",tfq2.getAngle());
//--------
transformStamped[3] = tfBuffer.lookupTransform("flange","link3", ros::Time(0));
ROS_INFO_STREAM("--------------------------Trasformazione da link 3 a flangia--------------------------\n");


ROS_INFO_STREAM("Vettore traslazione: " << transformStamped[3].transform.translation);

tf2::Quaternion tfq3(transformStamped[3].transform.rotation.x,transformStamped[3].transform.rotation.y,transformStamped[3].transform.rotation.z,transformStamped[3].transform.rotation.w);

tf2::Matrix3x3 m3(tfq3);


ROS_INFO("Matrice di rotazione :\n");
c1=m3.getColumn(0);
c2=m3.getColumn(1);
c3=m3.getColumn(2);
ROS_INFO("%f  %f  %f\n",c1.x(),c2.x(),c3.x());
ROS_INFO("%f  %f  %f\n",c1.y(),c2.y(),c3.y());
ROS_INFO("%f  %f  %f\n",c1.z(),c2.z(),c3.z());



m3.getEulerYPR(y,p,r);
ROS_INFO("\nAngoli di eulero (Radianti) y >%f<, p >%f<, r >%f<\n", y, p, r);


var=tfq3.getAxis();
ROS_INFO("Rappresentazione asse-angolo:\n");
ROS_INFO("ASSE: [%f  %f  %f]:",var.x(),var.y(),var.z());
ROS_INFO("\nANGOLO:%f",tfq3.getAngle());
//-----------
transformStamped[4] = tfBuffer.lookupTransform("flange","link2", ros::Time(0));
ROS_INFO_STREAM("--------------------------Trasformazione da link 2 a flangia--------------------------\n");


ROS_INFO_STREAM("Vettore traslazione: " << transformStamped[4].transform.translation);

tf2::Quaternion tfq4(transformStamped[4].transform.rotation.x,transformStamped[4].transform.rotation.y,transformStamped[4].transform.rotation.z,transformStamped[4].transform.rotation.w);

tf2::Matrix3x3 m4(tfq4);


ROS_INFO("Matrice di rotazione :\n");
c1=m4.getColumn(0);
c2=m4.getColumn(1);
c3=m4.getColumn(2);
ROS_INFO("%f  %f  %f\n",c1.x(),c2.x(),c3.x());
ROS_INFO("%f  %f  %f\n",c1.y(),c2.y(),c3.y());
ROS_INFO("%f  %f  %f\n",c1.z(),c2.z(),c3.z());



m4.getEulerYPR(y,p,r);
ROS_INFO("\nAngoli di eulero (Radianti) y >%f<, p >%f<, r >%f<\n", y, p, r);


var=tfq4.getAxis();
ROS_INFO("Rappresentazione asse-angolo:\n");
ROS_INFO("ASSE: [%f  %f  %f]:",var.x(),var.y(),var.z());
ROS_INFO("\nANGOLO:%f",tfq4.getAngle());
//--------
transformStamped[5] = tfBuffer.lookupTransform("flange","link1", ros::Time(0));
ROS_INFO_STREAM("--------------------------Trasformazione da link 1 a flangia--------------------------\n");


ROS_INFO_STREAM("Vettore traslazione: " << transformStamped[5].transform.translation);

tf2::Quaternion tfq5(transformStamped[5].transform.rotation.x,transformStamped[5].transform.rotation.y,transformStamped[5].transform.rotation.z,transformStamped[5].transform.rotation.w);

tf2::Matrix3x3 m5(tfq5);


ROS_INFO("Matrice di rotazione :\n");
c1=m5.getColumn(0);
c2=m5.getColumn(1);
c3=m5.getColumn(2);
ROS_INFO("%f  %f  %f\n",c1.x(),c2.x(),c3.x());
ROS_INFO("%f  %f  %f\n",c1.y(),c2.y(),c3.y());
ROS_INFO("%f  %f  %f\n",c1.z(),c2.z(),c3.z());



m5.getEulerYPR(y,p,r);
ROS_INFO("\nAngoli di eulero (Radianti) y >%f<, p >%f<, r >%f<\n", y, p, r);


var=tfq5.getAxis();
ROS_INFO("Rappresentazione asse-angolo:\n");
ROS_INFO("ASSE: [%f  %f  %f]:",var.x(),var.y(),var.z());
ROS_INFO("\nANGOLO:%f",tfq5.getAngle());
//----------
transformStamped[6] = tfBuffer.lookupTransform("flange","base_link", ros::Time(0));
ROS_INFO_STREAM("--------------------------Trasformazione da base_link a flangia--------------------------\n");


ROS_INFO_STREAM("Vettore traslazione: " << transformStamped[6].transform.translation);

tf2::Quaternion tfq6(transformStamped[6].transform.rotation.x,transformStamped[6].transform.rotation.y,transformStamped[6].transform.rotation.z,transformStamped[6].transform.rotation.w);

tf2::Matrix3x3 m6(tfq6);


ROS_INFO("Matrice di rotazione :\n");
c1=m6.getColumn(0);
c2=m6.getColumn(1);
c3=m6.getColumn(2);
ROS_INFO("%f  %f  %f\n",c1.x(),c2.x(),c3.x());
ROS_INFO("%f  %f  %f\n",c1.y(),c2.y(),c3.y());
ROS_INFO("%f  %f  %f\n",c1.z(),c2.z(),c3.z());



m6.getEulerYPR(y,p,r);
ROS_INFO("\nAngoli di eulero (Radianti) y >%f<, p >%f<, r >%f<\n", y, p, r);


var=tfq6.getAxis();
ROS_INFO("Rappresentazione asse-angolo:\n");
ROS_INFO("ASSE: [%f  %f  %f]:",var.x(),var.y(),var.z());
ROS_INFO("\nANGOLO:%f",tfq6.getAngle());

ROS_INFO("\nVALORI QUATERNIONI\n");
for(int k=0; k<7;k++){
    ROS_INFO_STREAM("Quaternione da link " << (6-k) << " a flangia");
ROS_INFO_STREAM("-------->: " << transformStamped[k].transform.rotation.x<<":::"<<transformStamped[k].transform.rotation.y<<":::"<<transformStamped[k].transform.rotation.z<<":::"<<transformStamped[k].transform.rotation.w);

}

} catch (tf2::TransformException &exception) {
ROS_WARN("%s", exception.what());
ros::Duration(1.0).sleep();
continue;
}
rate.sleep();
}



return 0;
};