
%% Da link6 a Flangia
%%Traslazione
tform_TOT6 = trvec2tform([0 0 -0.1])
%% Da link5 a Flangia
R5=rotx(-90); %%Nella variabile transformstamped le rotazioni vengono considerate 
%con angolo positivo se orarie con angolo negativo se antiorarie
tform5=rotm2tform(R5);

trasl_5 = trvec2tform([0 0 -0.1]);

tform_TOT5=trasl_5*tform5
%% Da link4 a Flangia
tform_TOT4 = trvec2tform([0 0 -0.96])
%% Da link3 a Flangia
R3=rotx(-90);
tform3=rotm2tform(R3);

trasl_3=trvec2tform([-0.15 0 -0.96]); 

tform_TOT3=trasl_3*tform3
%% Da link2 a Flangia (Ruoto prima lungo x in senso antiorario; dopo lungo y in senso antiorario)
R2_p=rotx(-90);
tform2_p=rotm2tform(R2_p);

R2=roty(-90);
tform2=rotm2tform(R2);

trasl_2=trvec2tform([-0.94 0 -0.96]);

tform_TOT2=trasl_2*tform2*tform2_p %%Regola di composizione che scrive da dx verso sx (Terna corrente) mettendo prima rotazione

%% Da link1 a Flangia
R1=roty(90);
tform1=rotm2tform(R1);

trasl_1=trvec2tform([-1.46 0 -1.11]);

tform_TOT1=trasl_1*tform1



