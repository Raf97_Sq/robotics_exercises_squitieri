#include <ros/ros.h>
#include <std_msgs/String.h>

#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"

#include "std_msgs/Int32MultiArray.h"
#include <pk2/messaggio.h>

int main(int argc,char **argv){
    ros::init(argc,argv,"publish");
    ros::NodeHandle nh;
    ros::Publisher chatterPublisher= nh.advertise<pk2::messaggio>("jointsPosition",1);
    ros::Rate loopRate(19);
    
    unsigned int count=0;
    while(ros::ok()){
    pk2::messaggio array;
    //Pulisco l'array dove saranno memorizzate le posizioni dei 6 giunti
    //array.data.clear();
    //Simulazione lettura posizioni
    


      array.pos1=(rand() % 90);
      array.pos2=(rand() % 90);
      array.pos3=(rand() % 90);
      array.pos4=(rand() % 20);
      array.pos5=(rand() % 20);
      array.pos6=(rand() % 20);
           
    
    //Pubblico l' array
        
    chatterPublisher.publish(array);
    
    ROS_INFO("I published something!");
        ros::spinOnce();
        loopRate.sleep();
       
    }
    return 0;
}