#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>

#include "ros/ros.h"

#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Int32MultiArray.h"

#include <pk2/messaggio.h>

int Arr[6];
void arrayCallback1(const pk2::messaggio::ConstPtr& array);

int main(int argc, char **argv)
{

  ros::init(argc, argv, "Subscriber");

  ros::NodeHandle n;  

  ros::Subscriber sub3 = n.subscribe("jointsPosition", 100, arrayCallback1);

    //Metto in attesa il flusso di esecuzione fin quando non si verifica l'evento callback che aggiorna il vettore con le posizioni nuove

  ros::spin();




  return 0;
}

void arrayCallback1(const pk2::messaggio::ConstPtr& array)
{

  int i = 0;
  // Stampiamo le posizioni ricevute dei giunti

ROS_INFO("%d\n ",array->pos1);
ROS_INFO("%d\n ",array->pos2);
ROS_INFO("%d\n ",array->pos3);
ROS_INFO("%d\n ",array->pos4);
ROS_INFO("%d\n ",array->pos5);
ROS_INFO("%d\n ",array->pos6);

 
return;
}